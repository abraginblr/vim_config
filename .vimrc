" Syntax highlighting
syntax on
set tabstop=4
set shiftwidth=4
set smarttab
set et

" Turn of auto indentation for new line
set ai
set cin
set showmatch
set listchars=tab:··
set list

" Visual bell
set visualbell

" Highlight search result
set hlsearch

set laststatus=2
